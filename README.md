# tango-cs docker image

tango-cs [docker] image to run a minimal [Tango control system](https://www.tango-controls.org).

The following device servers are started by default (using supervisor):

* DataBaseds
* Starter
* TangoAccessControl
* TangoTest

Note that if you only need the Tango Database server, you can use the `registry.gitlab.com/tango-controls/docker/tango-db` image. See [tango-db](https://gitlab.com/tango-controls/docker/tango-db).

## Usage

To use this image, you need a working instance of mysql with the tango tables created.

```console
docker run -it --rm --name tango-cs \
  -e ORB_PORT=10000 \
  -e TANGO_HOST=127.0.0.1:10000 \
  -e MYSQL_HOST=mysql:3306 \
  -e MYSQL_USER=tango \
  -e MYSQL_PASSWORD=tango \
  -e MYSQL_DATABASE=tango \
  registry.gitlab.com/tango-controls/docker/tango-cs:9
```

The recommended way is to use `docker-compose` to start the mysql container:

```yaml
version: '3.7'
services:
  mysql:
    image: registry.gitlab.com/tango-controls/docker/mysql:5
    environment:
     - MYSQL_ROOT_PASSWORD=root
  tango-cs:
    image: registry.gitlab.com/tango-controls/docker/tango-cs:9
    ports:
     - "10000:10000"
    environment:
     - TANGO_HOST=localhost:10000
     - MYSQL_HOST=mysql:3306
     - MYSQL_USER=tango
     - MYSQL_PASSWORD=tango
     - MYSQL_DATABASE=tango
    depends_on:
     - mysql
```

Run `docker-compose up`. The Tango Database server will be available on `localhost:10000`.

[docker]: https://www.docker.com
