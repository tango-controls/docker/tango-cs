FROM condaforge/miniforge3 as conda

ENV CPPTANGO_VERSION 9.3.5
ENV TANGO_DATABASE_VERSION 5.17
ENV TANGO_TEST_VERSION 3.4
ENV TANGO_ADMIN_VERSION 1.16
ENV TANGO_STARTER_VERSION 7.7
ENV TANGO_ACCESS_CONTROL_VERSION 2.17

RUN conda create -y -p /tango \
    cpptango=$CPPTANGO_VERSION \
    tango-database=$TANGO_DATABASE_VERSION \
    tango-admin=$TANGO_ADMIN_VERSION \
    tango-starter=$TANGO_STARTER_VERSION \
    tango-access-control=$TANGO_ACCESS_CONTROL_VERSION \
    tango-test=$TANGO_TEST_VERSION \
  && conda clean -afy

COPY copy-bin-deps.sh /

# Only keep required binaries and their shared library dependencies
RUN /copy-bin-deps.sh /tango Databaseds /tango-slim \
   &&  /copy-bin-deps.sh /tango tango_admin /tango-slim \
   &&  /copy-bin-deps.sh /tango Starter /tango-slim \
   &&  /copy-bin-deps.sh /tango TangoAccessControl /tango-slim \
   &&  /copy-bin-deps.sh /tango TangoTest /tango-slim \
   && rm -rf /tango \
   && mv /tango-slim /tango

FROM debian:11-slim

LABEL maintainer="TANGO Controls Team <contact@tango-controls.org>"

RUN apt-get update \
  && apt-get install -y wait-for-it supervisor \
  && rm -rf /var/lib/apt/lists/*

COPY --from=conda /tango /tango
COPY resources/tango_register_device /tango/bin/
COPY resources/supervisord.conf      /etc/supervisord.conf

ENV PATH /tango/bin:$PATH
ENV ORB_PORT=10000
ENV TANGO_HOST=127.0.0.1:${ORB_PORT}

EXPOSE ${ORB_PORT}

RUN useradd -m tango
USER tango

CMD /usr/bin/wait-for-it $MYSQL_HOST --timeout=30 --strict -- \
    /usr/bin/supervisord -c /etc/supervisord.conf
